package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.Calendar;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels.PeminjamanViewModel;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.adapters.BukuAdapter;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.alarm.AlertReceiver;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.Buku;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.Peminjaman;

public class DetailBukuActivity extends AppCompatActivity {

    private TextView editTextJudul;
    private TextView editTextPenulis;
    private TextView editTextKeterangan;
    private TextView editTextLokasi;
    private ImageView imageBukuDetail;
    private Button buttonPinjamBuku;
//    private LinearLayout layout;
    private RelativeLayout layout;

    private PeminjamanViewModel peminjamanViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_buku);
        init();


        final Intent intent = getIntent();
        if (!intent.hasExtra("BUKU_ID") || !intent.hasExtra("BUKU_JUDUL") || !intent.hasExtra("BUKU_LOKASI")
        || !intent.hasExtra("BUKU_KETERANGAN") || !intent.hasExtra("BUKU_PENULIS")) {
            return;
        }

        editTextJudul.setText(intent.getStringExtra("BUKU_JUDUL"));
        editTextLokasi.setText(intent.getStringExtra("BUKU_LOKASI"));
        editTextKeterangan.setText(intent.getStringExtra("BUKU_KETERANGAN"));
        editTextPenulis.setText(intent.getStringExtra("BUKU_PENULIS"));
        setImg(intent.getStringExtra("BUKU_IMAGE_URL"));

        buttonPinjamBuku.setOnClickListener(new View.OnClickListener() {
            private String judul = intent.getStringExtra("BUKU_JUDUL");
            private String penulis = intent.getStringExtra("BUKU_PENULIS");
            private String img_url = intent.getStringExtra("BUKU_IMAGE_URL");

            @Override
            public void onClick(View view) {
                peminjamanViewModel.insert(new Peminjaman(judul, penulis, img_url, false));
                Log.d("PINJAM", "onClick: pinjam on proces...");

                // alarm
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, 1);
                startAlarm(judul, calendar);
            }

            private void startAlarm(String namaBuku, Calendar cal){
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
                intent.putExtra("namaBuku", namaBuku);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Log.d("alarm", "startAlarm: starting.......");
                    Log.d("alarm", "startAlarm: millis........."+ cal.getTimeInMillis());
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
                    Log.d("alarm", "startAlarm: zz...........");
                }
            }
        });
    }

    private void setImg(String cloudinaryUrl) {
        Glide.with(this.getApplicationContext()).asBitmap().load(cloudinaryUrl).into(imageBukuDetail);
    }

    private void init() {
        layout = findViewById(R.id.linearLayoutDetail);
        editTextJudul = findViewById(R.id.textViewJudul);
        editTextPenulis = findViewById(R.id.textViewPenulis);
        editTextKeterangan = findViewById(R.id.textViewKeterangan);
        editTextLokasi = findViewById(R.id.textViewLokasi);
        imageBukuDetail = findViewById(R.id.imageBukuDetail);
        buttonPinjamBuku = findViewById(R.id.pinjam_buku);
        peminjamanViewModel = ViewModelProviders.of(this).get(PeminjamanViewModel.class);
    }
}
