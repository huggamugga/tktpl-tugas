package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels.BukuViewModel;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.adapters.BukuAdapter;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.Buku;

public class MainActivity extends AppCompatActivity {
    private BukuViewModel bukuViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarwidget);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_homepage);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final BukuAdapter bukuAdapter = new BukuAdapter();
        recyclerView.setAdapter(bukuAdapter);

        FloatingActionButton floatingButton = findViewById(R.id.floating_add_buku);
        floatingButton.setOnClickListener(floatAddListener);

        bukuViewModel = ViewModelProviders.of(this).get(BukuViewModel.class);
        bukuViewModel.getAllBukus().observe(this, new Observer<List<Buku>>() {
            @Override
            public void onChanged(List<Buku> bukus) {
                // update RecyclerView
                bukuAdapter.setBukus(bukus);
            }
        });

        bukuAdapter.setOnItemClickListener(new BukuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Buku buku) {
                // implement later
                Intent intent = new Intent(getApplicationContext(), DetailBukuActivity.class);
                intent.putExtra("BUKU_ID", buku.getId());
                intent.putExtra("BUKU_JUDUL", buku.getJudul());
                intent.putExtra("BUKU_KETERANGAN", buku.getKeterangan());
                intent.putExtra("BUKU_LOKASI", buku.getLokasi());
                intent.putExtra("BUKU_PENULIS", buku.getPenulis());
                intent.putExtra("BUKU_IMAGE_URL", buku.getImg_url_cloudinary());

                startActivity(intent);

            }
        });

    }

    private View.OnClickListener floatAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), AddBukuActivity.class);
            startActivity(intent);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.peminjamanButton) {
            Intent intent = new Intent(getApplicationContext(), PeminjamanActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
