package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.Peminjaman;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.PeminjamanDao;

@Database(entities = {Buku.class, Peminjaman.class}, version = 5)
public abstract class BukuDatabase extends RoomDatabase {

    private static BukuDatabase instance;

    public abstract BukuDao bukuDao();
    public abstract PeminjamanDao peminjamanDao();

    public static synchronized BukuDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), BukuDatabase.class, "buku_database")
                    .fallbackToDestructiveMigration()
//                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private BukuDao bukuDao;

        private PopulateDbAsyncTask(BukuDatabase bukuDatabase) {
            bukuDao = bukuDatabase.bukuDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //bukuDao.insert(new Buku("Slam Dunk 22", "Takehiko Inoue", "Jakarta","keren", "https://d28hgpri8am2if.cloudfront.net/book_images/cvr9781421533292_9781421533292_hr.jpg"));
            //bukuDao.insert(new Buku("Buku Matematika Kelas 10", "B.K. Noormandiri", "Medan","pinter", "http://togamas.com/css/images/items/potrait/JPEG_6145_Matematika_untuk_SMA_MA_Kelas_XI_Kelompok_Wajib_Jilid_2_K13__.jpg"));
            return null;
        }
    }



}
