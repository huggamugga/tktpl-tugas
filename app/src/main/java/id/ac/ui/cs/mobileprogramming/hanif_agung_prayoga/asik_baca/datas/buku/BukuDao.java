package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BukuDao {

    @Insert
    void insert(Buku buku);

    @Update
    void update(Buku buku);

    @Delete
    void delete(Buku buku);

    @Query("DELETE FROM buku")
    void deleteAllBukus();

    @Query("SELECT * FROM buku")
    LiveData<List<Buku>> getAllBukus();
 }
