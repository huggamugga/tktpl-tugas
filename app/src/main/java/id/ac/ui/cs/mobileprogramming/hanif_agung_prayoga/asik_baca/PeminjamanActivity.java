package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels.PeminjamanViewModel;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.adapters.PeminjamanAdapter;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.Peminjaman;

public class PeminjamanActivity extends AppCompatActivity {

    private PeminjamanViewModel peminjamanViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.peminjaman);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_homepage);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final PeminjamanAdapter peminjamanAdapter = new PeminjamanAdapter();
        recyclerView.setAdapter(peminjamanAdapter);

        peminjamanViewModel = ViewModelProviders.of(this).get(PeminjamanViewModel.class);
        peminjamanViewModel.getAllPeminjamans().observe(this, new Observer<List<Peminjaman>>() {
            @Override
            public void onChanged(List<Peminjaman> bukus) {
                // update RecyclerView
                peminjamanAdapter.setPeminjamans(bukus);
            }
        });

    }
}
