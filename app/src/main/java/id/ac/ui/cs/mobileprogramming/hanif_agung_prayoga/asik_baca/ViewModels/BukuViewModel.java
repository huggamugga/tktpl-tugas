package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.Buku;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.BukuRepository;

public class BukuViewModel extends AndroidViewModel {
    private BukuRepository bukuRepository;
    private LiveData<List<Buku>> allBukus;

    public BukuViewModel(@NonNull Application application) {
        super(application);
        bukuRepository = new BukuRepository(application);
        allBukus = bukuRepository.getAllBukus();
    }

    public void insert(Buku buku) {
        bukuRepository.insert(buku);
    }

    public void delete(Buku buku) {
        bukuRepository.delete(buku);
    }

    public void update(Buku buku) {
        bukuRepository.update(buku);
    }

    public void deleteAllBukus() {
        bukuRepository.deleteAllBukus();
    }

    public LiveData<List<Buku>> getAllBukus() {
        return allBukus;
    }


}
