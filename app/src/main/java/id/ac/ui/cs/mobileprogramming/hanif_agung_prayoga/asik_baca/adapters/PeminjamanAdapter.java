package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.R;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.Peminjaman;

public class PeminjamanAdapter extends RecyclerView.Adapter<PeminjamanAdapter.PeminjamanHolder> {
    private List<Peminjaman> peminjamans = new ArrayList<>();
    private OnItemClickListener listener;
    private Context context;

    @NonNull
    @Override
    public PeminjamanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item_peminjaman_view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.peminjaman_item, parent, false);
        this.context = parent.getContext();
        return new PeminjamanHolder(item_peminjaman_view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeminjamanHolder holder, int position) {
        Peminjaman currentPeminjaman = peminjamans.get(position);
        holder.textViewJudulPeminjaman.setText(currentPeminjaman.getJudul());
        holder.textViewPenulisPeminjaman.setText(currentPeminjaman.getPenulis());
        if (currentPeminjaman.isConfirmed()) {
            holder.textViewStatusPeminjaman.setText("Sudah dikonfirmasi");
        } else {
            holder.textViewStatusPeminjaman.setText("Belum dikonfirmasi");
        }
        setImg(currentPeminjaman, holder);
    }

    private void setImg(Peminjaman peminjaman, PeminjamanHolder holder) {
        Glide.with(this.context.getApplicationContext()).asBitmap().load(peminjaman.getImg_url_cloudinary()).into(holder.imageViewCoverDetailPeminjaman);
    }

    @Override
    public int getItemCount() {
        Log.d("SIZE", "getItemCount: "+ peminjamans.size());
        return peminjamans.size();
    }

    public void setPeminjamans(List<Peminjaman> peminjamans) {
        this.peminjamans = peminjamans;
        notifyDataSetChanged();
    }

    class PeminjamanHolder extends RecyclerView.ViewHolder {
        private TextView textViewJudulPeminjaman;
        private TextView textViewPenulisPeminjaman;
        private TextView textViewStatusPeminjaman;
        private ImageView imageViewCoverDetailPeminjaman;


        public PeminjamanHolder(@NonNull View itemView) {
            super(itemView);

            textViewJudulPeminjaman = itemView.findViewById(R.id.judulDetailPeminjaman);
            textViewPenulisPeminjaman = itemView.findViewById(R.id.penulisDetailPeminjaman);
            textViewStatusPeminjaman = itemView.findViewById(R.id.confirmedDetailPeminjaman);
            imageViewCoverDetailPeminjaman = itemView.findViewById(R.id.coverDetailPeminjaman);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(peminjamans.get(position));
                    }
                }
            });
        }
    }


    public interface OnItemClickListener {
        void onItemClick(Peminjaman peminjaman);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
