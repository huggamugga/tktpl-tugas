package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String namaBuku = intent.getStringExtra("namaBuku");
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder builder = notificationHelper.getChannelNotification(namaBuku);
        notificationHelper.getManager().notify(1, builder.build());
    }
}
