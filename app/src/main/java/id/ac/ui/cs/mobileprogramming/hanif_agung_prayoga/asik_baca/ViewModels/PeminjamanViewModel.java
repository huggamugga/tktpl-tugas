package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.Peminjaman;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman.PeminjamanRepository;


public class PeminjamanViewModel extends AndroidViewModel {
    private PeminjamanRepository peminjamanRepository;
    private LiveData<List<Peminjaman>> allPeminjamans;

    public PeminjamanViewModel(@NonNull Application application) {
        super(application);
        peminjamanRepository = new PeminjamanRepository(application);
        allPeminjamans = peminjamanRepository.getAllPeminjamans();
    }

    public void insert(Peminjaman peminjaman) {
        peminjamanRepository.insert(peminjaman);
    }

    public void delete(Peminjaman peminjaman) {
        peminjamanRepository.delete(peminjaman);
    }

    public void update(Peminjaman peminjaman) {
        peminjamanRepository.update(peminjaman);
    }

    public void deleteAllPeminjamans() {
        peminjamanRepository.deleteAllPeminjamans();
    }

    public LiveData<List<Peminjaman>> getAllPeminjamans() {
        return allPeminjamans;
    }


}
