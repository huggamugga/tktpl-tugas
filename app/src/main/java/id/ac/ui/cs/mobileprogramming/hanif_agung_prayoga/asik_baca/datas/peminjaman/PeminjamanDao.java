package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PeminjamanDao {

    @Insert
    void insert(Peminjaman peminjaman);

    @Update
    void update(Peminjaman peminjaman);

    @Delete
    void delete(Peminjaman peminjaman);

    @Query("DELETE FROM peminjaman_self")
    void deleteAllPeminjamans();

    @Query("SELECT * FROM peminjaman_self")
    LiveData<List<Peminjaman>> getAllPeminjamans();

}
