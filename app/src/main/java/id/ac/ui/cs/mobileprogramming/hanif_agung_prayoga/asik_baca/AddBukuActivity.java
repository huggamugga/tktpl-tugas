package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.cloudinary.Cloudinary;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.utils.ObjectUtils;

import org.cloudinary.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ViewModels.BukuViewModel;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.Buku;

public class AddBukuActivity extends AppCompatActivity {
    private EditText editTextJudul;
    private EditText editTextPenulis;
    private EditText editTextKeterangan;
    private EditText editTextLokasi;
    private BukuViewModel bukuViewModel;
    private Button submit;
    private Button pilihGambar;
    private ImageView holderImage;
    private Uri selectedUri;

    final int REQUEST_CODE_GALLERY = 999;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_buku);
        init();
        MediaManager.init(this);

        pilihGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        AddBukuActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            String cloudinaryUrl;
            @Override
            public void onClick(View v) {
                try {
                    final String judul = editTextJudul.getText().toString();
                    final String lokasi = editTextLokasi.getText().toString();
                    final String penulis = editTextPenulis.getText().toString();
                    final String keterangan = editTextKeterangan.getText().toString();
                    final String imgStr = "stringggg";

                    Toast.makeText(getApplicationContext(), "Saving...", Toast.LENGTH_SHORT).show();

                    MediaManager.get().upload(selectedUri)
                            .unsigned("lltndmb8")
                            .option("resource_type", "image")
                            .callback(new UploadCallback() {
                                @Override
                                public void onStart(String requestId) {
                                    Log.d("CLOUDINARY", "onStart: starting.....");
                                }

                                @Override
                                public void onProgress(String requestId, long bytes, long totalBytes) {

                                }

                                @Override
                                public void onSuccess(String requestId, Map resultData) {
                                    cloudinaryUrl = resultData.get("secure_url").toString();
                                    Log.d("CLOUDINARY", "onSuccess: succeed");
                                    Log.d("CLOUDINARY", "onSuccess: "+ cloudinaryUrl);

                                    bukuViewModel.insert(new Buku(judul,penulis, lokasi, keterangan, imgStr, cloudinaryUrl));
                                }

                                @Override
                                public void onError(String requestId, ErrorInfo error) {

                                }

                                @Override
                                public void onReschedule(String requestId, ErrorInfo error) {

                                }
                            }).dispatch();

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            selectedUri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(selectedUri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                holderImage.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init() {
        bukuViewModel = new BukuViewModel(getApplication());
        editTextJudul = findViewById(R.id.input_judul);
        editTextPenulis = findViewById(R.id.input_penulis);
        editTextKeterangan = findViewById(R.id.input_keterangan);
        editTextLokasi = findViewById(R.id.input_lokasi);
        submit = findViewById(R.id.button_submit_buku);
        pilihGambar = findViewById(R.id.button_pilih_gambar);
        holderImage = findViewById(R.id.imgHolderUpload);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Intent nowIntent = new Intent(this, this.getClass());
            startActivity(nowIntent);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Intent nowIntent = new Intent(this, this.getClass());
            startActivity(nowIntent);
        }
    }
}
