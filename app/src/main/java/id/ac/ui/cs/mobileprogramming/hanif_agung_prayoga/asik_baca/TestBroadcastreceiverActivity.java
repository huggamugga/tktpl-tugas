package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.alarm.AlertReceiver;

public class TestBroadcastreceiverActivity extends AppCompatActivity {
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "default" ;
    private Button fivesecsButton;
    private Button tensecsButton;
    private Button fifteensecsButton;
    Calendar calendar = Calendar.getInstance();


    // alarm
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    public static final int REQUEST_CODE=101;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_broadcast_receiver);

        fivesecsButton = findViewById(R.id.button5detik);
        tensecsButton = findViewById(R.id.button10detik);
        fifteensecsButton = findViewById(R.id.button15detik);


        fivesecsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 1);
            startAlarm("Slam dunk", calendar);
            }
        });

    }


    private void startAlarm(String namaBuku, Calendar cal){
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
        intent.putExtra("namaBuku", namaBuku);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d("alarm", "startAlarm: starting.......");
            Log.d("alarm", "startAlarm: millis........."+ cal.getTimeInMillis());
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            Log.d("alarm", "startAlarm: zz...........");
        }
    }




}
