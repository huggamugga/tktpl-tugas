package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.ImageUtil;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.R;
import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.Buku;

public class BukuAdapter extends RecyclerView.Adapter<BukuAdapter.BukuHolder> {
    private List<Buku> bukus = new ArrayList<>();
    private OnItemClickListener listener;
    private Context context;

    @NonNull
    @Override
    public BukuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item_buku_view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.buku_item, parent, false);
        this.context = parent.getContext();
        return new BukuHolder(item_buku_view);
    }


    @Override
    public void onBindViewHolder(@NonNull BukuHolder holder, int position) {
        Buku currentBuku = bukus.get(position);
        holder.textViewJudul.setText(currentBuku.getJudul());
        holder.textViewPenulis.setText(currentBuku.getPenulis());
        holder.textViewLokasi.setText(currentBuku.getLokasi());
        setImg(currentBuku, holder);
    }

    private Bitmap setImage(Buku buku) {
        Uri img_uri = Uri.parse(buku.getImg_url());
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.context.getContentResolver(), img_uri);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setImg(Buku buku, BukuHolder holder) {
        Glide.with(this.context.getApplicationContext()).asBitmap().load(buku.getImg_url_cloudinary()).into(holder.imageViewCover);
    }


    @Override
    public int getItemCount() {
        Log.d("SIZE", "getItemCount: "+ bukus.size());
        return bukus.size();
    }

    public void setBukus(List<Buku> bukus) {
        this.bukus = bukus;
        notifyDataSetChanged();
    }


    class BukuHolder extends RecyclerView.ViewHolder {
        private TextView textViewJudul;
        private TextView textViewPenulis;
        private TextView textViewLokasi;
        private ImageView imageViewCover;
        private Context context;
        private RelativeLayout parentLayout;


        public BukuHolder(@NonNull View itemView) {
            super(itemView);
            textViewJudul = itemView.findViewById(R.id.judul);
            textViewPenulis = itemView.findViewById(R.id.penulis);
            textViewLokasi = itemView.findViewById(R.id.lokasi);
            imageViewCover = itemView.findViewById  (R.id.cover);
            parentLayout = itemView.findViewById(R.id.parentLayoutDetailBuku);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(bukus.get(position));
                    }
                }
            });

        }
    }

    public interface OnItemClickListener {
        void onItemClick(Buku buku);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
