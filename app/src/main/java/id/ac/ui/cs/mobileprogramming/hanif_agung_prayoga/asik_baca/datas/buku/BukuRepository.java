package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class BukuRepository {

    private BukuDao bukuDao;
    private LiveData<List<Buku>> allBukus;

    public BukuRepository(Application application) {
        BukuDatabase bukuDatabase = BukuDatabase.getInstance(application);
        bukuDao = bukuDatabase.bukuDao();
        allBukus = bukuDao.getAllBukus();
    }

    public LiveData<List<Buku>> getAllBukus() {
        return allBukus;
    }

    public void insert(Buku buku) {
        new InsertBukuAsyncTask(bukuDao).execute(buku);
    }

    public void delete(Buku buku) {
        new DeleteBukuAsyncTask(bukuDao).execute(buku);
    }

    public void update(Buku buku) {
        new UpdateBukuAsyncTask(bukuDao).execute(buku);
    }

    public void deleteAllBukus() {
        new DeleteAllBukusAsyncTask(bukuDao).execute();
    }

    private static class InsertBukuAsyncTask extends AsyncTask<Buku, Void, Void> {
        private BukuDao bukuDao;

        private InsertBukuAsyncTask(BukuDao bukuDao) {
            this.bukuDao = bukuDao;
        }

        @Override
        protected Void doInBackground(Buku... bukus) {
            bukuDao.insert(bukus[0]);
            return null;
        }
    }

    private static class DeleteBukuAsyncTask extends AsyncTask<Buku, Void, Void> {
        private BukuDao bukuDao;

        private DeleteBukuAsyncTask(BukuDao bukuDao) {
            this.bukuDao = bukuDao;
        }

        @Override
        protected Void doInBackground(Buku... bukus) {
            bukuDao.delete(bukus[0]);
            return null;
        }
    }

    private static class UpdateBukuAsyncTask extends AsyncTask<Buku, Void, Void> {
        private BukuDao bukuDao;

        private UpdateBukuAsyncTask(BukuDao bukuDao) {
            this.bukuDao = bukuDao;
        }

        @Override
        protected Void doInBackground(Buku... bukus) {
            bukuDao.update(bukus[0]);
            return null;
        }
    }

    private static class DeleteAllBukusAsyncTask extends AsyncTask<Void, Void, Void> {
        private BukuDao bukuDao;

        private DeleteAllBukusAsyncTask(BukuDao bukuDao) {
            this.bukuDao = bukuDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            bukuDao.deleteAllBukus();
            return null;
        }
    }


}
