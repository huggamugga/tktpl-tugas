package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "peminjaman_self")
public class Peminjaman {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String judul;
    private String penulis;
    private String img_url_cloudinary;
    private boolean confirmed;

    public Peminjaman(String judul, String penulis, String img_url_cloudinary, boolean confirmed) {
        this.judul = judul;
        this.penulis = penulis;
        this.img_url_cloudinary = img_url_cloudinary;
        this.confirmed = confirmed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getImg_url_cloudinary() {
        return img_url_cloudinary;
    }

    public void setImg_url_cloudinary(String img_url_cloudinary) {
        this.img_url_cloudinary = img_url_cloudinary;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }
}
