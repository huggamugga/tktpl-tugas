package id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.peminjaman;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.hanif_agung_prayoga.asik_baca.datas.buku.BukuDatabase;

public class PeminjamanRepository {
    private PeminjamanDao peminjamanDao;
    private LiveData<List<Peminjaman>> allPeminjamans;

    public PeminjamanRepository(Application application) {
        BukuDatabase bukuDatabase = BukuDatabase.getInstance(application);
        peminjamanDao = bukuDatabase.peminjamanDao();
        allPeminjamans = peminjamanDao.getAllPeminjamans();
    }

    public LiveData<List<Peminjaman>> getAllPeminjamans() {
        return allPeminjamans;
    }

    public void insert(Peminjaman peminjaman) {
        new InsertPeminjamanAsyncTask(peminjamanDao).execute(peminjaman);
    }

    public void delete(Peminjaman peminjaman) {
        new DeletePeminjamanAsyncTask(peminjamanDao).execute(peminjaman);
    }

    public void update(Peminjaman peminjaman) {
        new UpdatePeminjamanAsyncTask(peminjamanDao).execute(peminjaman);
    }

    public void deleteAllPeminjamans() {
        new DeleteAllPeminjamansAsyncTask(peminjamanDao).execute();
    }

    private static class InsertPeminjamanAsyncTask extends AsyncTask<Peminjaman, Void, Void> {
        private PeminjamanDao peminjamanDao;

        private InsertPeminjamanAsyncTask(PeminjamanDao peminjamanDao) {
            this.peminjamanDao = peminjamanDao;
        }

        @Override
        protected Void doInBackground(Peminjaman... peminjamans) {
            peminjamanDao.insert(peminjamans[0]);
            return null;
        }
    }

    private static class DeletePeminjamanAsyncTask extends AsyncTask<Peminjaman, Void, Void> {
        private PeminjamanDao peminjamanDao;

        private DeletePeminjamanAsyncTask(PeminjamanDao peminjamanDao) {
            this.peminjamanDao = peminjamanDao;
        }

        @Override
        protected Void doInBackground(Peminjaman... peminjamans) {
            peminjamanDao.delete(peminjamans[0]);
            return null;
        }
    }

    private static class UpdatePeminjamanAsyncTask extends AsyncTask<Peminjaman, Void, Void> {
        private PeminjamanDao peminjamanDao;

        private UpdatePeminjamanAsyncTask(PeminjamanDao peminjamanDao) {
            this.peminjamanDao = peminjamanDao;
        }

        @Override
        protected Void doInBackground(Peminjaman... peminjamans) {
            peminjamanDao.update(peminjamans[0]);
            return null;
        }
    }

    private static class DeleteAllPeminjamansAsyncTask extends AsyncTask<Void, Void, Void> {
        private PeminjamanDao peminjamanDao;

        private DeleteAllPeminjamansAsyncTask(PeminjamanDao peminjamanDao) {
            this.peminjamanDao = peminjamanDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            peminjamanDao.deleteAllPeminjamans();
            return null;
        }
    }
}
